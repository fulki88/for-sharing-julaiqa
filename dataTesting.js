const map = [
  6529, 6637, 6456, 6677, 6794,
];

const reduce = {
  euy: 108,
};

const reduce2 = {
  npk: 6637,
  nama: 'Fajar',
  umur: 32,
  temanTerdekat: 'Nuvai',
  gengMissqueen: true,
};

const filter = [
  {
    npk: 6529,
    nama: 'Febe',
    umur: 23,
    temanTerdekat: 'Rafdi',
    gengMissqueen: true,
  }, {
    npk: 6794,
    nama: 'Farhad',
    umur: 24,
    temanTerdekat: 'Rafdi',
    gengMissqueen: false,
  },
];

const compilation = {
  euy: 19925,
};

module.exports = {
  map,
  reduce,
  reduce2,
  filter,
  compilation,
};
