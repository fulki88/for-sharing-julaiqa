const jawaban = require('dataTesting.js');


// penamaan ini hanya fiktif belaka, jika ada kesamaan nama,
// umur dan npk, saya selaku programmer memohon maaf.


describe('Describe', async () => {
  test('test untuk JulaiQA -- Map, Reduce, Filter', async () => {
    const korban = [
      {
        npk: 6529,
        nama: 'Febe',
        umur: 23,
        temanTerdekat: 'Rafdi',
        gengMissqueen: true,
      },
      {
        npk: 6637,
        nama: 'Fajar',
        umur: 32,
        temanTerdekat: 'Nuvai',
        gengMissqueen: true,
      },
      {
        npk: 6456,
        nama: 'Aman',
        umur: 2,
        temanTerdekat: 'Renni',
        gengMissqueen: false,
      },
      {
        npk: 6677,
        nama: 'Hilmi',
        umur: 27,
        temanTerdekat: 'Mahe',
        gengMissqueen: true,
      },
      {
        npk: 6794,
        nama: 'Farhad',
        umur: 24,
        temanTerdekat: 'Rafdi',
        gengMissqueen: false,
      },
    ];
    
    // what you need
    // [ 6529, 6637, 6456, 6677, 6794 ];

    expect(npkKorban, `SALAH! Jawaban anda: ${npkKorban}`).toEqual(jawaban.map);
    // console.log(npkKorban);



    // what you need
    // 108

    expect(totalUmur, `SALAH! Jawaban anda: ${totalUmur}`).toEqual(jawaban.reduce.euy);
    // console.log(totalUmur);



    // what you need
    // {
    //   npk: 6637,
    //   nama: 'Fajar',
    //   umur: 32,
    //   temanTerdekat: 'Nuvai',
    //   gengMissqueen: true,
    // };

    expect(palingtua, `SALAH! Jawaban anda: ${JSON.stringify(palingtua)}`).toEqual(jawaban.reduce2);
    // console.log(palingtua);


    // what you need
    // [
    //   {
    //     npk: 6529,
    //     nama: 'Febe',
    //     umur: 23,
    //     temanTerdekat: 'Rafdi',
    //     gengMissqueen: true,
    //   }, {
    //     npk: 6794,
    //     nama: 'Farhad',
    //     umur: 24,
    //     temanTerdekat: 'Rafdi',
    //     gengMissqueen: false,
    //   },
    // ];


    expect(cariTemanTerdekat, `SALAH! Jawaban anda: ${JSON.stringify(cariTemanTerdekat)}`).toEqual(jawaban.filter);
    // console.log(cariTemanTerdekat);


    // what you need
    // 19925

    expect(totalAngkagengMissqueen, `SALAH! Jawaban anda: ${totalAngkagengMissqueen}`).toEqual(jawaban.compilation.euy);
    // console.log(totalAngkagengMissqueen);
  });
});